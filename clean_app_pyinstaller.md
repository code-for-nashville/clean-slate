# Building a clean_app Executable
## _Building the executable_

At the moment the most reliable method for building an executable is the following, which preps the system to use `pyinstaller`:
```
$ pip install pyinstaller
$ pip install wxPython
$ pip install selenium
$ pip install webdriver_manager
```
This will install the needed packages on the build system (and not just in the pipenv). Once that is completed, run the following from the root of the source code directory
```
$ pyinstaller --onefile clean_app.py
```
This should generate a single file in the `.\dist\` directory.

## _Troubleshooting ModuleNotFoundErrors_
If your	executable errors at runtime with complaints of	missing	packages similar to

```
ModuleNotFoundError: No	module named 'webdriver_manager'
```

Update the `hiddenimports` line in the `clean_app.spec` file (created when running `pyinstaller`) following this example:

```
hiddenimports=['webdriver_manager']
```

with the list matching the whatever the `ModuleNotFoundError` indicates. Then try rebuilding your executable by running

```
$ pyinstaller --onefile clean_app.spec
```

And try	the newly built	exectable once more. You may need to repeat the	process	to ensure all the `hiddenimports` are covered.
