# clean-slate
Web-scraping Shelby County Court Clerk website to support legal volunteers helping people get records expunged.

## Dependencies
- [python]
- [pipenv]

## Setup
Setup [pipenv] (if you haven't already).
Then, run:

```
pipenv install
```

## Usage
Once you're setup, you can run the application as a command-line or GUI.

For command line, simply run:

```
pipenv run python clean_slate.py
```

If you get tired of entering the prompts, then setup your `.env` file:
- Copy the [example.env] file and rename to `.env`.
- Add your values after the equal signs (as appropriate).


For a GUI, simply run:

```
pipenv run python clean_app.py
```

## Credits
- Project Manager: Vienna Thompkins (@viennav)
- Maintainer: Tim Eccleston (@combinatorist)
- Creator: Mat Allen (@bramallama)
- Contributors: Gavin Schriver (@gavinschriver), Wesley Duffee-Braun (@wduffee)

[python]: https://www.python.org/
[pipenv]: https://realpython.com/pipenv-guide/
[example.env]: ./example.env
