import wx
import datetime
import wx.adv
from selenium import webdriver
import re
import csv
from os import getenv
from os import path
from os import makedirs
from getpass import getpass
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from webdriver_manager.chrome import ChromeDriverManager


""" Script function definitions """

# START OF THE login_portal FUNCTION

def login_portal(username, password):
 
    driver.get(site)
    
    user_name_entry = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.ID, 'UserName')))

    ## User Info entry and click login
    user_name_entry.send_keys(username)
    password_entry = driver.find_element_by_id('Password')
    password_entry.send_keys(password)
    driver.find_element_by_css_selector('.btn').click()
    
    
    #login_confirmation_wait = WebDriverWait(driver, 30).until(
    #EC.presence_of_element_located((By.CLASS_NAME, 'btn-description')))
    search_site = 'https://cjs.shelbycountytn.gov/CJS/Home/Dashboard/29'
    driver.get(search_site)

    # END OF THE login_portal FUNCTION

# START OF THE search_and_expunge FUNCTION

## Enter search of site
def search_and_expunge():

    record_search = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.ID, 'caseCriteria_SearchCriteria')))
    
    record_search.send_keys(name_or_case_number)
    driver.find_element_by_id("btnSSSubmit").click()


    ## Entering in DOB    
    dob_drop_down = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, "//th[@data-field='DateOfBirthSort']//span[@title='Sort / Filter Options']"))).click()    
    
    dob_text_box = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, "//div[@class='k-animation-container']//input[@class='k-input']")))
    ##For some reason, if I get rid of this .click() below and placed it above like the others, I get
    ##AttributeError: 'NoneType' object has no attribute 'send_keys'    
    dob_text_box.click()
    
    dob_text_box.send_keys(date_of_birth)
    driver.find_element_by_xpath("//div[@class='k-animation-container']//button[@type='submit']").click()

    ##From name search results (after DOB is entered), gets to info to be downloaded
    ##From here I can find all the caseLinks and store them in a list, and then loop thru each.
    ##Each going click case# (@detailed), scrape info, press back, find next case#, repeat

    # END OF THE search_and_expunge FUNCTION



# START OF THE extend_dropdown FUNCTION
def extend_dropdown():

    def get_dropdown():
        global dropdown_results, actual_dropdown
        dropdown_results = driver.find_elements_by_xpath("//span[contains(@class, 'k-input')]")
        actual_dropdown = []

        #filters dropdowns into new list.
        for i in range(len(dropdown_results)):
            if dropdown_results[i].text == "10":
                actual_dropdown.append(dropdown_results[i])

    def click_and_change(i):
        global actions
        actions = ActionChains(driver)
        
        driver.execute_script("arguments[0].scrollIntoView(false);", actual_dropdown[i])
        actual_dropdown[i].click()
        click_amount = driver.find_elements_by_xpath("//li[@class='k-item' and text() = '25']")
        
        for i in range(len(click_amount)):
            if click_amount[i].text == "25":
                click_amount[i].click()


    #Bottom dropdown, list ALL results
    get_dropdown()
    click_and_change(len(actual_dropdown)-1)

    #>10 table results
    get_dropdown()
    if len(actual_dropdown) >= 1:
        for i in range(len(actual_dropdown)):
            click_and_change(i)

    # END OF THE extend_dropdown FUNCTION



    
# START OF THE case_numbers FUNCTION
## Here for multiple case numbers
def case_numbers():

    # check to ensure the results directory exists, and create it otherwise
    if not path.exists("results"):
        makedirs("results")

    #case_numbers_wait = WebDriverWait(driver, 30).until(
    #EC.presence_of_element_located((By.CLASS_NAME, 'caseLink' )))
    
    case_links = driver.find_elements_by_class_name('caseLink')

    #save a shot of the page so we know what it looks like after trying to entery the DOB
    page = driver.find_element_by_tag_name("body")
    page.screenshot("results/case_search_results_page.png")

    #save the page HTML for further investigation 
    content = driver.page_source

    with open('results/case_results_page.html', 'w') as f:

        f.write(content)
    
    if len(case_links) == 0:
        print(f'No cases matching this DOB: {date_of_birth}')


    with open('results/results.csv', 'w', newline = '') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['First Name', 'Last Name', "DoB", "Case Number", "Disp Date", 'Charge', "Result", ])
            
    for i in range(len(case_links)):
        actions.move_to_element(case_links[i]).perform() ##Had an issue with this on the larger runs
        case_links[i].click()
        
        ##Here is where the downloading starts
        scrape_case_number = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.XPATH, "//div[@id='divCaseInformation_body']//span[contains(text(), 'Case Number')]/parent::*")))
                                        
        disp_number = driver.find_elements_by_id('DispositionEventsPrintSection')
        disp_rows = driver.find_elements_by_xpath("//*[contains(@id, 'CriminalDispositions')]/table/tbody/tr")
        disp_cells = driver.find_elements_by_xpath("//*[contains(@id, 'CriminalDispositions')]/table/tbody/tr/td")

        disp_date_regex = re.compile(r'(\d\d.\d\d.\d\d\d\d) Disposition')
        
        a = 1
        b = 2

        if len(disp_number) > 0:
            first_name = name_or_case_number.split(", ")[1].title()
            last_name = name_or_case_number.split(", ")[0].title()
                
            for i in range(len(disp_rows)):
                disp_date_match = disp_date_regex.search(disp_number[0].text) #This is here because it throws a fit when there are no dispo events (if in an active/open case).
                
                with open('results/results.csv', 'a', newline = '') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerow([first_name, last_name, date_of_birth, scrape_case_number.text, disp_date_match.group(1), disp_cells[a].text, disp_cells[b].text])
                    a += 3
                    b += 3
                
        else:
            with open('results.csv', 'a', newline = '') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerow([first_name, last_name, date_of_birth, "No Dispo info found"])                
            
        ##This goes back to search results
        driver.find_element_by_xpath("//p[contains(text( ), 'Search Results')]").click()

    # END OF THE case_numbers FUNCTION


# START OF THE ApplicationWindow CLASS DEFINITION
# This hold's the UI and calls functions based on user input
class ApplicationWindow(wx.Frame):

    def __init__(self, parent, title): 

        # ApplicationWindow instatiation
        super(ApplicationWindow, self).__init__(parent, title = title,size = (750,500))

        # Setup the panel which is primarily composed of three BoxSizers
        # main_sizer: the sizer that fills the whole panel
        # input_sizer: the sizer that contains all the inputs (except the button)
        # 
        
        panel = wx.Panel(self) 
        main_sizer = wx.BoxSizer(wx.VERTICAL)     
        
        # All the Text Labels
        #
        self.top_label = wx.StaticText(panel, -1, "Clean Slate Helper Appication")
        self.top_label.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.username_label = wx.StaticText(panel, -1, "Shelby County CJS Username:")
        self.password_label = wx.StaticText(panel, -1, "Shelby County CJS Password:") 
        self.search_label = wx.StaticText(panel, -1, "Enter search name as last, first middle suffix (ex. smith, john f) or leave blank to use demo search values")
        self.search_label.Wrap(350)
        self.datepicker_label = wx.StaticText(panel, -1, "Please enter search date of birth (or leave unchanged to use demo search values)")
        self.datepicker_label.Wrap(350)
        self.bottom_message_label = wx.StaticText(panel, -1, "You must click the above button to continue. Pressing enter will not work.")
        self.bottom_message_label.SetFont(wx.Font(13, wx.SWISS, wx.NORMAL, wx.BOLD))

        # All the Controls (ie, inputs)
        #
        self.username_textctrl = wx.TextCtrl(panel, -1, "")
        self.password_textctrl = wx.TextCtrl(panel, -1, style = wx.TE_PASSWORD)
        self.search_textctrl = wx.TextCtrl(panel,-1, "")
        self.datepicker_controller = wx.adv.DatePickerCtrl(panel, style = wx.adv.DP_DROPDOWN)
        self.datepicker_controller.SetValue(wx.DateTime.FromDMY(7, 10, 1981))    # This is November 7, 1981. Note that months increment from 0, so November is 10

        # Build the UI by adding the text labels and controls to the sizers in order
        # Labels at the top of the window is added directly to the main_sizer, with a staticline separating them
        #         
        main_sizer.Add(self.top_label, 0, wx.ALL, 5)
        main_sizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        # Flexible grid for the four inputs (username, password, search string, calendar) and their labels
        # Items are added in a top down method, from left to right each row according to the number of columns
        #
        input_sizer = wx.FlexGridSizer(cols=2,hgap=5, vgap=5)
        input_sizer.AddGrowableCol(1)        
        input_sizer.Add(self.username_label, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL) 
        input_sizer.Add(self.password_label, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL) 
        input_sizer.Add(self.username_textctrl, 0, wx.EXPAND)
        input_sizer.Add(self.password_textctrl, 0, wx.EXPAND)
        input_sizer.Add(self.search_label, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL) 
        input_sizer.Add(self.datepicker_label, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL) 
        input_sizer.Add(self.search_textctrl, 0, wx.EXPAND)
        input_sizer.Add(self.datepicker_controller, 0, wx.EXPAND)

        # Add the input_sizer to the main_sizer
        #
        main_sizer.Add(input_sizer, 0, wx.EXPAND|wx.ALL, 10)        

        # Box for the Login and Search button
        # Button is currently the only element with a Bind call 
        # So it is the only way to move forward in the UI
        #
        main_sizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)
        login_button = wx.Button(panel, 0,"Login and Search") 
        main_sizer.Add(login_button,0,wx.ALL,5)
        login_button.Bind(wx.EVT_BUTTON,self.OnClicked) 
        main_sizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        # Box for bottom messages and alerts (although it doesn't seem to work for most of the updates yet?)
        main_sizer.Add(self.bottom_message_label, 0, wx.ALL, 5)
        
        # main_sizer is complete, let's assign it to the main panel
        panel.SetSizer(main_sizer) 

        self.Centre() 
        self.Show() 
        self.Fit()

    
    # Function called when the button is clicked in the UI
    def OnClicked(self, event): 

        global name_or_case_number
        global date_of_birth

        self.bottom_message_label.SetLabel(f"Logging into the Shelby CJS with username {self.username_textctrl.GetValue()}")

        # If the search_textctrl field is blank then the user wants to search with default values, no need to change anything

        if self.search_textctrl.GetValue() == "":
            pass
        else:
            name_or_case_number = self.search_textctrl.GetValue()
            month_selected =  self.datepicker_controller.GetValue().GetMonth()
            day_selected =  self.datepicker_controller.GetValue().GetDay()
            year_selected =  self.datepicker_controller.GetValue().GetYear()
            date_of_birth = str(month_selected+1) + "/" + str(day_selected) + "/" + str(year_selected)

        login_portal(self.username_textctrl.GetValue(), self.password_textctrl.GetValue())

        print(f"Running the search for {name_or_case_number}, DOB {date_of_birth}")

        self.bottom_message_label.SetLabel(f"Running the search for {name_or_case_number}, DOB {date_of_birth}")

        search_and_expunge()
        extend_dropdown()
        case_numbers()

        print("Completed. Please review the results.csv file in the results directory")
        self.bottom_message_label.SetLabel("Completed. Please review the \"results.csv\" file in the results directory")

    # END OF THE ApplicationWindow CLASS DEFINITION


""" Program Execution """

options = Options()

site = 'https://cjs.shelbycountytn.gov/CJS/Account/Login'
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
is_captcha_detected = True

name_or_case_number = 'smith, john'
date_of_birth = "11/07/1981"

clean_slate_app = wx.App() 
ApplicationWindow(None,  'Clean Slate Helper Application')
clean_slate_app.MainLoop()

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
## 
## DEVELOPER NOTES
## 
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
##

#before I forget, it'd be cool if there's no captcha to not need to hit enter

## So I want to look insdie the DispoPrint Selection, find which headers contain "disposition", and then get each tr that under the repective heads
## TR's contain each line that I'm looking for
## [table] role="table"/Tbody/tr/td(can skip 1st td[just a number])
## This is giving a start, works with erry page so far. Need to refine. dispo_test = driver.find_elements_by_xpath("//*[contains(@id, 'CriminalDispositions')]/table/tbody/tr/td")

##So, i can run a command to see how many tr's there are, and that's how many new lines will be added to the csv.
##There's gotta be an easy way to designate which td's will go where. Let's go simple fn.
## I'll need nested for loop. For each cacse number will have multiple tr's (get length of tr's) for each charge [charge=tr?]

##prob is too big rn. I've got case#, DOB, name, dspoDate, just need Charges and Finding.
##ok, one loop per dispo_row
##Then just place 1 and 2 in the remaining spots.
##But I need to make sure that it's going thru the whole list, not just rep'ing

##
##I want to pressure test this
## I can test to make sure this works by removing the dob and therefore hitting multiple people.

## Also looks like one person can have more than 10 cases and that makes a new page, gotta check for that. Check if person has >10 cases, if so click button to change case list
## Looks like some ppl don't have dispo events, line 92 gets tripped. If statement out.
##      This one^^ has an "H" in the case#, also says "active or open" in status
## Might be a smarter/more effective idea to search the status and see if it's "Active or Open", if so, abort
